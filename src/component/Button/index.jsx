import { Component } from 'react';
import './style.scss';

export default class Button extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
        <button className='button' id = {this.props.id} data-id = {this.props.modalId} onClick={this.props.onClick} style = {{backgroundColor: this.props.bgColor, color: this.props.color, width:this.props.width, height:this.props.height}}>{this.props.btnText}</button>
    )
  }
}
