import { Component } from 'react';
import './style.scss'

export default class Modal extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
      <>
      <div className='background-modal' onClick={this.props.closeModal}>
        <div className='modal' style={{backgroundColor: this.props.bgColor}}>
          <div className="header-modal">
            <p className="title-modal">{this.props.title}</p>
            {this.props.closeButton &&  <button onClick={this.props.close} id = {this.props.closeId} className="close-modal">X</button>}
          </div>
          <p className="text-modal">{this.props.text}</p>
          <div className="modal-btn">
            {this.props.actions}
          </div>
        </div>
      </div>
      </>
    )
  }
}
