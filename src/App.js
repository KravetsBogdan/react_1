import logo from './logo.svg';
import './App.css';
import Button from './component/Button';
import Modal from './component/Modal';
import React, { Component } from 'react'
import modalObj from './modalObject';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      openModal: false,
      modalInfo: {
         text: '',
         header: '',
         closeButton: '',
         firstBtn: '',
         secondBtn: ''
      },
   } 
  }

  
  
  openModal = (e) => {
    const modalId = parseInt(e.currentTarget.dataset.id);
    const modal = modalObj.find((item) => item.id === modalId)
    if(modal){
      this.setState({modalInfo: {...modal}, openModal: true})
    }
  }
  closeModal = (e) =>{
    if(e.target.id === 'cancel' || e.target.id === 'ok' || e.target.className === 'background-modal' || e.target.className === 'close-modal'){
      this.setState({openModal: false})
    }
  }
  render() {
    return (
      <>
        <Button onClick = {this.openModal} modalId = {1} btnText = 'Open first modal' bgColor = 'blue' color = 'white' />
        <Button onClick = {this.openModal} modalId = {2} btnText = 'Open second modal' color = 'white' bgColor = 'green'/>
        {this.state.openModal &&  <Modal  closeModal = {this.closeModal}  closeButton = {this.state.modalInfo.closeButton} title = {this.state.modalInfo.header}  text = {this.state.modalInfo.text} actions = {<>
          <Button  btnText = 'OK' bgColor ={'#a00000'} color = 'white' id = 'ok' width={150}/>
          <Button  btnText = 'Cancel' bgColor ={'#a00000'} color = 'white' id = 'cancel' width={150}/>
        </>}/>}
      </>
    );
  }
}
